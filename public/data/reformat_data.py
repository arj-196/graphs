from __future__ import division
import json
import sys, traceback
from datetime import datetime
import os
import re
import csv
import json
import operator
import shapes
from format_graph_vis import VisGraphFormatData

startTime = datetime.now()


def aggregate_topics(AGGREGATE_TOPICS, location):
    print "Aggregating Topics", datetime.now() - startTime
    formatdata = VisGraphFormatData()

    for T in AGGREGATE_TOPICS['target']:
        print T, datetime.now() - startTime
        root = T['path']
        topics = formatdata.get_topics(root)
        data = formatdata.get_data_for_topics(topics)
        domains = formatdata.get_unique_domains(data, maxsize=10000)

        file_out = open(location + T['name'] + '.csv', 'w')
        SEP = ','
        NEWLINE = '\n'
        for d in domains:
            string = d['id'] + SEP + str(d['size']) + SEP + d['optional']['tag'] + NEWLINE
            string = string.encode('utf8')
            file_out.write(string)
        file_out.flush()

    print "done", datetime.now() - startTime


def main():
    AGGREGATE_TOPICS = {
        'target': [
            {"path": 'raw/topics1/', "name": "rapport_a_la_banque"},
        ]
    }
    aggregate_topics(AGGREGATE_TOPICS, 'raw/topics_global/')


if __name__ == '__main__':
    main()
