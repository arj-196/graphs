from __future__ import division
import json
import sys, traceback
from datetime import datetime
import os
import re
import csv
import json
import operator
from format_graph import FormatData
import shapes
import random


class VisGraphFormatData(FormatData):
    def compute_nodes_and_edges(self, data, domains, topics, matrix):
        nodes = []
        edges = []
        domain_nodes = {}
        domain_top_topic = []
        topic_color = {}
        tc = 0
        # creating nodes
        for i, domain in enumerate(domains):
            majority_topic = domain['majoritytopic'][0][0]
            if majority_topic not in domain_nodes or len(
                    domain_nodes[majority_topic]) < MAXNODES:

                # topic color
                if majority_topic not in topic_color:
                    topic_color[majority_topic] = COLOR_GROUPS[tc]
                    tc += 1
                group_color = topic_color[majority_topic]

                nodes.append({
                    "id": i,
                    "label": domain['name'],
                    "size": domain['size'] + 3,
                    "group": majority_topic,
                    "color": group_color
                })

                if majority_topic not in domain_nodes:
                    domain_nodes[majority_topic] = [i]
                else:
                    domain_nodes[majority_topic].append(i)

                domain_top_topic.append((i, majority_topic))


        j = 0
        for topic in topics:
            if j not in topic_color:
                topic_color[j] = COLOR_GROUPS[tc]
                tc += 1

            group_color = topic_color[j]
            nodes.append({
                "id": 't' + str(j),
                "label": topic['name'].upper(),
                "size": 3,
                "group": j,
                "color": BACKGROUNDCOLOR,
                "font": {
                    "size": 19,
                    "background": group_color
                }
            })
            j += 1

        # edges : domain -> topic
        for d in domain_top_topic:
            edges.append({
                "from": d[0],
                "to": 't' + str(d[1]),
                "color": BACKGROUNDCOLOR
            })

        # edges : random : domain -> domain
        for i, domain in enumerate(domains):
            # if random.random < 0.8:
            #     nbEdges = random.randint(1, 3)
            # else:
            #     nbEdges = 1

            nbEdges = random.randint(1, 10)

            randEdge = random.sample(range(len(nodes)), nbEdges)
            # randEdge = random.sample(range(len(domain_nodes[domain['majoritytopic'][0][0]])), nbEdges)
            for _re in randEdge:
                edges.append({
                    "from": i,
                    "to": _re,
                    "color": "#E6E6E6",
                    "physics": False,
                })

        return nodes, edges


def drawVisGraph(root):
    formatdata = VisGraphFormatData()
    topics = formatdata.get_topics(root)
    data = formatdata.get_data_for_topics(topics)
    domains = formatdata.get_unique_domains(data, maxsize=20)

    matrix = formatdata.compute_matrix(data, domains)
    domains = formatdata.get_highest_topic_in_domains(domains, matrix)
    nodes, edges = formatdata.compute_nodes_and_edges(data, domains, topics, matrix)
    return nodes, edges


BACKGROUNDCOLOR = '#ffffff'
COLOR_GROUPS = [
    '#97C2FC',
    '#7BE141',
    '#FB7E81',
    '#FFFF00',
    '#AD85E4',
    '#EB7DF4',
    '#6E6EFD',
    '#FFA807',
]
MAXNODES = 30


def main():
    # # linear graph
    # domains, formatted_matrix, formatted_topics = drawlineargraph()

    # polygon graph
    nodes, edges = drawVisGraph('raw/T1/')

    print
    json.dump({
        "nodes": nodes,
        "edges": edges
    }, open("formatted/topic_global_1.json", 'wb'))


MAXENTRIES = 70
startTime = datetime.now()
if __name__ == '__main__':
    if sys.argv[1:]:
        pass
    else:
        main()
        # test()

    print "--- end", datetime.now() - startTime
