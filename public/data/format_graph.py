from __future__ import division
import json
import sys, traceback
from datetime import datetime
import os
import re
import csv
import json
import operator
import shapes
import copy
startTime = datetime.now()


class FormatData(object):
    def get_topics(self, root):
        """
        Get topic data from files
        :return:
        """
        topics = []
        for f in os.listdir(root):
            if os.path.isfile(root + f):

                name = f.split('.')[0]
                topics.append({
                    "name": re.sub("_", " ", name),
                    "file": open(root + f, 'rb'),
                    "id": name
                })
        return topics

    def get_data_for_topics(self, topics):
        """
        Per topic read data from files
        :param topics:
        :return:
        """
        topicdata = []
        for t in topics:
            with t["file"] as csvfile:
                data = []
                spamreader = csv.reader(csvfile, delimiter=",")
                for row in spamreader:
                    size = float(row[1])
                    data.append({
                        "name": row[0],
                        "size": size
                        , "tag": row[2]
                    })

            topicdata.append(data)
        return topicdata

    def format_domain_name(self, text):
        """
        format domain names to remove ptns
        :param text:
        :return:
        """
        ptns = [
            'http[s]?://www\.',
            'http[s]?://',
            'http[s]?:',
        ]
        formatted_text = text
        for p in ptns:
            formatted_text = re.sub(p, '', formatted_text)
        return formatted_text

    def get_unique_domains(self, data, maxsize=5, minsize=1):
        """
        Calculate unique domains and their counts
        :param minsize:
        :param maxsize:
        :param data:
        :return:
        """
        unique_domains = {}
        domains = []
        for topic in data:
            for d in topic:
                _id = self.format_domain_name(d["name"]).split('/')[0]
                if _id not in unique_domains:
                    MAXSIZE_NAME_SIZE = 200
                    if len(_id) > MAXSIZE_NAME_SIZE:
                        name = _id[0:MAXSIZE_NAME_SIZE] + "..."
                    else:
                        name = _id
                    domains.append({"name": name, 'id': _id, "size": d['size'], "optional": d})
                    unique_domains[_id] = len(domains) - 1
                else:
                    # if domains[unique_domains[_id]]['size'] < maxsize:
                    domains[unique_domains[_id]]['size'] += d['size']

        # normalizing values
        sizes = [d['size'] for d in domains]
        maxind = max(sizes)
        minind = min(sizes)

        def normalize(_d):
            newsize = (_d['size'] - minind) * (maxsize - minsize)
            newsize = newsize / (maxind - minind)
            newsize += minsize
            _d['size'] = newsize
            return _d
        map(normalize, domains)

        sorted_domains = sorted(domains, key=lambda k: k['size'], reverse=True)
        return sorted_domains

    def set_domain_coordinates(self, domains, topics, shape, g):
        """
        For unique domains set their coordinates
        :param g:
        :param topics:
        :param domains:
        :param shape:
        :return:
        """
        coord = shape.get_coordinates()
        for i, v in enumerate(domains):
            if i < len(coord):
                domains[i]['x'] = coord[i]['x']
                domains[i]['y'] = coord[i]['y']
        return domains

    def compute_matrix(self, data, domains):
        """
        Compute domain to topic matrix
        :param data:
        :param domains:
        :return:
        """
        matrix = []
        for topic in data:
            submatrix = [0] * len(domains)
            for d in topic:
                try:
                    _id = self.format_domain_name(d["name"]).split('/')[0]
                    index = next(index for (index, dt) in enumerate(domains) if dt["id"] == _id)
                    submatrix[index] += int(d['size'])
                except:
                    pass
            matrix.append(submatrix)
        return matrix

    def get_highest_topic_in_domains(self, domains, matrix):
        """
        Calcuate highest topic per domain
        :param domains:
        :param matrix:
        :return:
        """
        for i, d in enumerate(domains):
            scores = {}
            for j, m in enumerate(matrix):
                scores[j] = m[i]
            domains[i]['majoritytopic'] = sorted(scores.items(), key=operator.itemgetter(1), reverse=True)
        return domains

    def formatdata(self, matrix, data, topics, shape):
        """
        Format matrix and domain data for sigma.js module
        :param matrix:
        :param data:
        :param topics:
        :param shape:
        :return:
        """
        formatted_matrix = []
        for submatrix in matrix:
            formatted_sub_matrix = []
            for i, v in enumerate(submatrix):
                if v > 0:
                    formatted_sub_matrix.append({"domain": i, "size": v})
                else:
                    formatted_sub_matrix.append(None)
            formatted_matrix.append(formatted_sub_matrix)

        formatted_topics = []
        coord = shape.get_coordinates()
        for i, d in enumerate(data):
            print "img for topic", d
            formatted_topics.append({
                "name": topics[d]["name"],
                "size": 100,  # TODO calculate size for topics
                "x": coord[i]['x'],
                "y": coord[i]['y'],
                "img": "img/" + d + ".png"
            })

        return formatted_matrix, formatted_topics


class LinearGraphFormatData(FormatData):
    pass


class PolygonGraphFormatData(FormatData):
    def set_domain_coordinates(self, domains, topics, shape, g):
        """
        For unique domains set their coordinates
        :param g:
        :param topics:
        :param domains:
        :param shape:
        :return:
        """

        topic_coordiantes = []
        for topic in topics:
            topic_coordiantes.append({
                "coords": shape(topic['x'], topic['y'], 0.7, 18).get_coordinates(g),
                "count": 0
            })

        for i, v in enumerate(domains):
            topicindex = domains[i]['majoritytopic'][0][0]
            coord = topic_coordiantes[topicindex]
            if coord['count'] < len(coord['coords']):
                domains[i]['x'] = coord['coords'][coord['count']]['x']
                domains[i]['y'] = coord['coords'][coord['count']]['y']
                topic_coordiantes[topicindex]['count'] += 1
                pass
            else:
                domains[i]['x'] = -99
                domains[i]['y'] = -99

        return domains


def drawlineargraph():
    """
    Draw linear graph
    :return:
    """
    formatdata = LinearGraphFormatData()
    root = 'raw/9_dec/autres_v3/'
    topics = formatdata.get_topics(root)
    data = formatdata.get_data_for_topics(topics)
    domains = formatdata.get_unique_domains(data)
    shape = shapes.Linear(MAXENTRIES, 1, 1.5, -0.95, 0.70)
    domains = formatdata.set_domain_coordinates(domains, None, shape)
    matrix = formatdata.compute_matrix(data, domains)
    domains = formatdata.get_highest_topic_in_domains(domains, matrix)

    shape = shapes.Linear(len(data))
    formatted_matrix, formatted_topics = formatdata.formatdata(matrix, data, topics, shape)
    return domains, formatted_matrix, formatted_topics


def drawpolygongraph():
    """
    Draw polygon graph
    :return:
    """

    formatdata = PolygonGraphFormatData()
    root = 'raw/topics1/'
    topics = formatdata.get_topics(root)
    data = formatdata.get_data_for_topics(topics)
    domains = formatdata.get_unique_domains(data)

    matrix = formatdata.compute_matrix(data, domains)
    domains = formatdata.get_highest_topic_in_domains(domains, matrix)

    shape = shapes.Polygon(len(data))
    formatted_matrix, formatted_topics = formatdata.formatdata(matrix, data, topics, shape)

    global MAXENTRIES
    MAXENTRIES = 100
    gox, goy = shape.get_centeroid(shape.points)
    domains = formatdata.set_domain_coordinates(domains, formatted_topics, shapes.Circle,
                                                {"x": gox, "y": goy})

    return domains, formatted_matrix, formatted_topics


def main():
    # # linear graph
    # domains, formatted_matrix, formatted_topics = drawlineargraph()

    # polygon graph
    domains, formatted_matrix, formatted_topics = drawpolygongraph()

    print
    # json.dump({
    #     "PARAM": {
    #         "XMIN": -0.50,
    #         "XMAX": 1.5,
    #         "YMIN": -0.99,
    #         "YMAX": 0.50,
    #         "MAXENTRIES": MAXENTRIES
    #     },
    #     "domains": domains,
    #     "topics": formatted_topics,
    #     "matrix": formatted_matrix
    # }, open("formatted/polygon_topics1.json", 'wb'))


MAXENTRIES = 70

# test remove
if __name__ == '__main__':
    if sys.argv[1:]:
        pass
    else:
        main()
        # test()

    print "--- end", startTime - datetime.now()
