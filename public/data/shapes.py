import math


class Shape(object):
    pass


class Linear(Shape):
    length = None
    XMIN = None
    XMAX = None
    YMIN = None
    YMAX = None

    def __init__(self, length, XMIN=-1.0, XMAX=1.0, YMIN=-1.0, YMAX=1.0):
        self.length = length
        self.XMIN = XMIN
        self.XMAX = XMAX
        self.YMIN = YMIN
        self.YMAX = YMAX

    def get_coordinates(self):
        coords = [{}] * self.length
        step = abs(self.YMIN - self.YMAX) / self.length
        for i in range(0, self.length):
            coords[i] = {
                'x': self.XMIN,
                'y': self.YMIN + (step * i)
            }

        return coords


class Polygon(Shape):
    sides = None
    points = None

    def __init__(self, sides):
        self.sides = sides

    def get_coordinates(self, radius=1.3, rotation=0, translation=None):
        one_segment = math.pi * 2 / self.sides

        points = [
            (math.sin(one_segment * i + rotation) * radius,
             math.cos(one_segment * i + rotation) * radius)
            for i in range(self.sides)]

        if translation:
            points = [[sum(pair) for pair in zip(point, translation)]
                      for point in points]

        ox, oy = self.get_centeroid(points)
        translateX = 1
        translateY = 0.2
        for i, p in enumerate(points):
            x = p[0]
            y = p[1]

            atcenter = False
            deltaX = abs(x - ox)
            if deltaX / radius < 0.5:
                atcenter = True

            if not atcenter:
                if x < ox and y < oy:
                    # south west
                    x -= translateX + 0.4
                    y -= translateY + 0.4
                    # print "sw"
                elif x > ox and y < oy:
                    # south east
                    x += translateX + 0.4
                    y -= translateY + 0.4
                    # print "se"
                elif x < ox and y > oy:
                    # north west
                    x -= translateX + 0.4
                    y += translateY
                    # print "nw"
                else:
                    # north east
                    x += translateX + 0.4
                    y += translateY
                    # print "ne"

            points[i] = (x, y)

        labeled_points = []
        for point in points:
            labeled_points.append({
                "x": point[0],
                "y": point[1] - 0.1
            })

        self.points = points
        return labeled_points

    def get_centeroid(self, points):
        singed_area = 0
        CX = 0
        CY = 0
        for i in range(0, len(points) - 1):
            xi1 = points[i][0]
            yi1 = points[i][1]
            xi2 = points[i + 1][0]
            yi2 = points[i + 1][1]
            singed_area += (xi1 * yi2) - (xi2 * yi1)
            CX += (xi1 + xi2) * (xi1 * yi2 - xi2 * yi1)
            CY += (yi1 + yi2) * (xi1 * yi2 - xi2 * yi1)

        singed_area *= 0.5
        CX *= (1 / (6 * singed_area))
        CY *= (1 / (6 * singed_area))

        return CX, CY


class Circle(Shape):
    def __init__(self, ox, oy, r, step):
        self.ox = ox
        self.oy = oy
        self.r = r
        self.step = step

    def get_coordinates(self, g):
        coordinates = []
        setpNS = 1
        angleNS = [90, 270]
        for a in range(0, 360, self.step):
            x = self.getx(self.ox, self.r, a)
            y = self.gety(self.oy, self.r, a)
            # local hemisphere
            if y > self.oy:
                lhemisphere = "n"
            else:
                lhemisphere = "s"
            # global hemisphere
            if y > g['y']:
                ghemisphere = "n"
            else:
                ghemisphere = "s"

            # translate north and south points
            if a % (90 * setpNS) == 0 and a != 0:
                setpNS += 2
                translateY = 0.16
                if lhemisphere == "n":
                    if ghemisphere == "n":
                        if y > 0:
                            y += translateY
                        else:
                            y -= translateY
                    else:
                        if y > 0:
                            y -= translateY
                        else:
                            y += translateY

                else:
                    if ghemisphere == "n":
                        if y > 0:
                            y -= translateY
                        else:
                            y += translateY
                    else:
                        if y > 0:
                            y += translateY
                        else:
                            y -= translateY

            # translate first angles before or after North and South
            translateX = 0.1
            translateY = 0.1
            for NS in angleNS:
                delta = a - NS
                if abs(delta) == self.step:
                    if lhemisphere == "s":
                        if delta > 0:
                            if ghemisphere == "n":
                                if y > 0:
                                    y -= translateY
                                else:
                                    y += translateY
                                if x > 0:
                                    x += translateX
                                else:
                                    x += translateX
                            else:
                                if y > 0:
                                    y += translateY
                                else:
                                    y -= translateY
                                if x > 0:
                                    x += translateX
                                else:
                                    x += translateX
                    else:
                        if delta > 0:
                            if ghemisphere == "n":
                                if y > 0:
                                    y += translateY
                                else:
                                    y -= translateY
                                if x > 0:
                                    x -= translateX
                                else:
                                    x -= translateX
                            else:
                                if y > 0:
                                    y -= translateY
                                else:
                                    y += translateY
                                if x > 0:
                                    x -= translateX
                                else:
                                    x -= translateX


            coordinates.append({
                "x": x,
                "y": y
            })

        return list(reversed(coordinates))

    def getx(self, ox, r, a):
        return ox + r * math.cos(self.getradian(a))

    def gety(self, oy, r, a):
        return oy + r * math.sin(self.getradian(a))

    @staticmethod
    def getradian(a):
        return a * (math.pi / 180)

# c = Circle(0, 0, 1, 36)
# import pprint
# pprint.pprint(c.get_coordinates())
