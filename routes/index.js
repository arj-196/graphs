var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/visjs/', function(req, res, next) {
    res.render("vis_network_basic.html")
});

module.exports = router;
